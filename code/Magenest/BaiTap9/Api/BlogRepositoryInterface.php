<?php
namespace Magenest\BaiTap9\Api;
use http\Message;

interface BlogRepositoryInterface
{
    /**
     * Return a filtered product.
     *
     * @param int $id
     * @return \Magenest\BaiTap9\Api\Data\ResponseItemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBlogById(int $id);
    /**
     * Return a filtered product.
     *
     * @param \Magenest\BaiTap9\Model\Api\ResponseItem $data
     * @return Magenest\BaiTap9\Model\Api\ResponseItem
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function addBlog(\Magenest\BaiTap9\Model\Api\ResponseItem $data);
    /**
     * Return a filtered product.
     *
     * @param \Magenest\BaiTap9\Model\Api\ResponseItem $data
     * @param int $id
     * @return Magenest\BaiTap9\Model\Api\ResponseItem
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateBlog(\Magenest\BaiTap9\Model\Api\ResponseItem $data, $id);
    /**
     * Return a succes message blog.
     *
     * @param int $id
     * @return Magenest\BaiTap9\Model\Api\ResponseItem
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteBlog($id);
}
