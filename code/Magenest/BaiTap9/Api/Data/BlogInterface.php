<?php
namespace Magenest\BaiTap9\Api\Data;

interface BlogInterface
{
    const DATA_ID = 'id';
    const DATA_TITLE = 'title';
    const DATA_DESCRIPTION = 'description';
    const DATA_CONTENT = 'content';
    const DATA_URL_REWRITE = 'url_rewrite';
    const DATA_STATUS = 'status';
    const DATA_CREATE_AT = 'create_at';
    const DATA_UPDATE_AT = 'update_at';
    const DATA_AUTHOR = 'author_id';

    /**
     * @return int
     */
    public function getId();
    /**
     * @return string
     */
    public function getTitle();
    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string|null
     */
    public function getContent();
    /**
     * @return string
     */
    public function getUrlRewrite();
    /**
     * @return int
     */
    public function getStatus();
    /**
     * @return string
     */
    public function getCreateAt();
    /**
     * @return string
     */
    public function getUpdateAt();
    /**
     * @return int
     */
    public function getAuthorId();
    /**
     * @return int
     */
    public function setId($id);
    /**
     * @return string
     */
    public function setTitle(string $title);
    /**
     * @return string
     */
    public function setDescription(string $description);

    /**
     * @return string|null
     */
    public function setContent(string $content);
    /**
     * @return string
     */
    public function setUrlRewrite(string $url);
    /**
     * @return int
     */
    public function setStatus(int $status);
    /**
     * @return string
     */
    public function setCreateAt( $create_at);
    /**
     * @return string
     */
    public function setUpdateAt(string $update_at);
    /**
     * @return int
     */
    public function setAuthorId(int $author_id);
}
