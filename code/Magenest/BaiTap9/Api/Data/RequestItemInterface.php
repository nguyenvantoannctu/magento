<?php
namespace Magenest\BaiTap9\Api\Data;
interface RequestItemInterface
{
    const DATA_ID = 'id';
    /**
     * @return int
     */
    public function getId();
    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id);
    /**
     * @param string $description
     * @return $this
     */
}
