<?php

namespace Magenest\BaiTap9\Block;

use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Template\Context;
use Magenest\BaiTap9\Model\Blog\ResourceModel\Collection\CollectionFactory;

class Details extends Template
{

    protected $_blogModel;
    protected $_blogResourceModel;


    public function __construct(Context $context,
        array $data = [],
        \Magenest\BaiTap9\Model\Blog\ListBlogFactory $blogModel,
                                \Magenest\BaiTap9\Model\Blog\ResourceModel\ListBlogFactory $blogResourceModel
                                )
    {
        $this->_blogModel = $blogModel;
        $this->_blogResourceModel = $blogResourceModel;
        parent::__construct($context, $data);
    }

    public function getRetails()
    {
        $blogConstant = $this->getRequest();
        $id_blog = $blogConstant->getParam('id');

        $model = $this->_blogModel->create();
        $blog = $this->_blogResourceModel->create()->load($model,$id_blog);

        return $model;
    }

}
