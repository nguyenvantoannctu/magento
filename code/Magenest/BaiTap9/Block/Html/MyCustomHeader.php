<?php

namespace Magenest\BaiTap9\Block\Html;

class MyCustomHeader extends \Magento\Theme\Block\Html\Header
{
    protected $_template = 'Magento_Theme::html/header.phtml';
    protected $_customerSession ;
    protected $_customerRepository;

    public function __construct(\Magento\Customer\Model\Session $customerSession,
                                \Magento\Framework\View\Element\Template\Context $context,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
                                array $data = [])
    {
        $this->_customerRepository = $customerRepositoryInterface;
        $this->_customerSession = $customerSession ;

        parent::__construct($context, $data);
    }


    /**
     * Custom Logic For Welcome Text
     *
     * @return string
     */
    public function getWelcome()
    {
            // get customer Name
            $customer = $this->_customerSession->getCustomer();
            $group_id = $customer->getGroupId();

            if($group_id == 7 && $this->_customerSession->isLoggedIn())
            {
                return parent::getWelcome() ;
            }
            else{
                return __('Hi Guest');
            }
    }
    public function getGroupID(){
        $customer = $this->_customerSession->getCustomer();
        $group_id = $customer->getGroupId();
        return $group_id;
    }
}
