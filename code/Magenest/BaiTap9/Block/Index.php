<?php

namespace Magenest\BaiTap9\Block;

use Magenest\BaiTap9\Model\Blog\ResourceModel\Collection\CollectionFactory;
use Magento\Framework\View\Element\Template;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $blogFactory;
    protected $modelblogFactory;
    protected $resoureUserFactory;
    protected $usermodelFacotry;
    public function __construct(Template\Context                                                        $context,
                                \Magenest\BaiTap9\Model\Blog\ResourceModel\ListBlogFactory              $_blogFactory,
                                \Magenest\BaiTap9\Model\Blog\ListBlogFactory                            $_modelblogFactory,


                                \Magento\User\Model\ResourceModel\UserFactory                           $_resoureUserFactory,
                                \Magento\User\Model\UserFactory                                         $_modelUserFactory,
                                array                                                                   $data = []
    )
    {
        $this->usermodelFacotry = $_modelUserFactory;
        $this->resoureUserFactory = $_resoureUserFactory;

        $this->modelblogFactory = $_modelblogFactory;
        $this->blogFactory = $_blogFactory;
        parent::__construct($context, $data);
    }
    public function getBlog(){
        $model = $this->modelblogFactory->create();
        $blog = $this->blogFactory->create()->load($model,1);
//        $this->getUser($model->getData('author_id'));
        return $model;
    }
    public function getUser(int $id){
        $model = $this->usermodelFacotry->create();
        $user = $this->resoureUserFactory->create()->load($model,$id);
        return $model;
    }

}
