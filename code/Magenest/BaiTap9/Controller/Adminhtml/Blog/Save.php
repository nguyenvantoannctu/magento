<?php

namespace Magenest\BaiTap9\Controller\Adminhtml\Blog;

use Magenest\BaiTap9\Model\Blog\ListBlogFactory;
use Magento\Backend\App\Action;

/**
 * Class Save
 * @package Magenest\Movie\Controller\Adminhtml\Director
 */
class Save extends Action
{
    /**
     * @var ListBlogFactory
     */
    private $postFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param ListBlogFactory $postFactory
     */
    public function __construct(
        Action\Context $context,
        ListBlogFactory $postFactory
    ) {
        parent::__construct($context);
        $this->postFactory = $postFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['id']) ? $data['id'] : null;

        $newData = [
            'title' => $data['title'],
            'url_rewrite' => $data['url_rewrite'],
        ];
        $post = $this->postFactory->create();

        if ($id) {
            $post->load($id);
        }
        try {
            $post->addData($newData);
            $post->save();
            $this->messageManager->addSuccessMessage(__('You saved the blog.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('backend/blog/index');
    }
}
