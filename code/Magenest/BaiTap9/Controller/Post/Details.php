<?php

namespace Magenest\BaiTap9\Controller\Post;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Details extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(Action\Context $context, PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $resultPage = $this->_pageFactory->create();
        return $resultPage;
    }
}
