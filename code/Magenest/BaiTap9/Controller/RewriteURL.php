<?php
namespace Magenest\BaiTap9\Controller;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\Context;


class RewriteURL extends Action
{
    protected $urlRewriteFactory;
    public function __construct(
        Context $context,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory)
    {
        $this->urlRewriteFactory = $urlRewriteFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = $data['id'];


        $urlRewrite = $this->urlRewriteFactory->create();

        /*if you want to rewrite url for “custom” set entity type*/
        $urlRewrite->setEntityType('custom');

        /*set current store ID */
        $urlRewrite->setStoreId(1);

        /*set 0 as this url is not created by system */
        $urlRewrite->setIsSystem(0);

        /* unique identifier - place random unique value to ID path */
        $urlRewrite->setIdPath(rand(1, 100000));

        /* set actual url path to target path field */
        $urlRewrite->setTargetPath("https://example.com/blog/view/id");

        /* set requested path which you want to create */
        $urlRewrite->setRequestPath("https://example.com/abc");

        /* set the type of Redirect */
        $urlRewrite->setRedirectType(301);

        /* save URL rewrite rule */
        $urlRewrite->save();
    }
}
