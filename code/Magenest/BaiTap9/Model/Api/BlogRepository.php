<?php
namespace Magenest\BaiTap9\Model\Api;
use Magenest\BaiTap9\Api\BlogRepositoryInterface;
use Magenest\BaiTap9\Api\Data\BlogInterface;
use Magenest\BaiTap9\Api\Data\RequestItemInterfaceFactory;
use Magenest\BaiTap9\Api\Data\ResponseItemInterfaceFactory;
use Magenest\BaiTap9\Model\Blog\ResourceModel\Collection\CollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\BaiTap9\Model\Blog\ListBlogFactory;
/**
 * Class BlogRepository
 */
class BlogRepository implements BlogRepositoryInterface
{
    /**
     * @var ModelFactory
     */
    private $blogModelFactory;
    /**
     * @var CollectionFactory
     */
    private $BlogCollectionFactory;
    /**
     * @var RequestItemInterfaceFactory
     */
    private $requestItemFactory;
    /**
     * @var ResponseItemInterfaceFactory
     */
    private $responseItemFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Resource DB
     */
    private $resource;
    /**
     * @var Connection DB
     */
    private $connection;
    /**
     * @param CollectionFactory $BlogCollectionFactory
     * @param RequestItemInterfaceFactory $requestItemFactory
     * @param ResponseItemInterfaceFactory $responseItemFactory
     * @param StoreManagerInterface $storeManager
     */

    public function __construct(
//        Action $BlogAction,
        ListBlogFactory              $blogModelFactory,
        CollectionFactory            $BlogCollectionFactory,
        RequestItemInterfaceFactory  $requestItemFactory,
        ResponseItemInterfaceFactory $responseItemFactory,
        StoreManagerInterface        $storeManager,
        \Magento\Framework\App\ResourceConnection $resource,

    )
    {
//        $this->BlogAction = $BlogAction;
        $this->resource = $resource;
        $this->connection = $resource->getConnection();
        $this->blogModelFactory = $blogModelFactory;
        $this->BlogCollectionFactory = $BlogCollectionFactory;
        $this->requestItemFactory = $requestItemFactory;
        $this->responseItemFactory = $responseItemFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritDoc}
     *
     * @param int $id
     * @return ResponseItemInterface
     * @throws NoSuchEntityException
     */
    public function getBlogById(int $id): mixed
    {
        $collection = $this->getBlogCollection();
        foreach ($collection as $items) {
            if ($items->getID() == $id) {
                $Blog = $items;
            }
        }
        /** @var BlogInterface $Blog */
        if (!$Blog->getId()) {
            throw new NoSuchEntityException(__('Blog not found'));
        }
        return $Blog;
    }

    public function addBlog($data): mixed
    {
        $title = $data['title'];
        $description = $data['description'];
        $content = $data['content'];
        $url_rewrite = $data['url_rewrite'];
        $status = $data['status'];
        $create_at = $data['create_at'];
        $update_at = $data['update_at'];
        $author_id = $data['author_id'];

        $insertData = $this->blogModelFactory->create();
        $insertData
            ->setTitle($title)
            ->setDescription($description)
            ->setContent($content)
            ->setUrlRewrite($url_rewrite)
            ->setStatus($status)
            ->setCreateAt($create_at)
            ->setUpdateAt($update_at)
            ->setAuthorId($author_id);
        $insertData->save();
        return $insertData;
    }

    public function updateBlog($data, $id): mixed
    {
        $blog = $this->getBlogById($id);

        $title = $data['title'];
        $description = $data['description'];
        $content = $data['content'];
        $url_rewrite = $data['url_rewrite'];
        $status = $data['status'];
        $create_at = $data['create_at'];
        $update_at = $data['update_at'];
        $author_id = $data['author_id'];

        $blog
            ->setTitle($title)
            ->setDescription($description)
            ->setContent($content)
            ->setUrlRewrite($url_rewrite)
            ->setStatus($status)
            ->setCreateAt($create_at)
            ->setUpdateAt($update_at)
            ->setAuthorId($author_id);
        $blog->save();
        return $blog;
    }

    public function getBlogCollection(): mixed
    {
        /** @var Collection $collection */
        $collection = $this->BlogCollectionFactory->create();
        return $collection;
    }

    public function deleteBlog($id): mixed
    {
//        // TODO: Implement deleteBlog() method.
//        try {
//            $model = $this->blogModelFactory->create();
//            $model->load($id);
//            $model->delete();
//        } catch (\Exception $e) {
//            $this->messageManager->addError($e->getMessage());
//        }
        $blog = $this->getBlogById($id);
        $table = $this->resource->getTableName('magenest_blog');
        $sql  = "DELETE FROM $table WHERE id=$id";
        if($this->connection->query($sql)){
            return $blog;
        }
        return throw new NoSuchEntityException(__('Blog have not delete'));
    }
}
