<?php
namespace Magenest\BaiTap9\Model\Api;
use Magenest\BaiTap9\Api\Data\ResponseItemInterface;
use Magento\Framework\DataObject;

/**
 * Class ResponseItem
 */
class ResponseItem extends DataObject implements ResponseItemInterface
{

    /**
     * @return int
     */
    public function getId(){
        return $this->_getData(self::DATA_ID);
    }
    /**
     * @return string
     */
    public function getTitle(){
        return $this->_getData(self::DATA_TITLE);

    }
    /**
     * @return string
     */
    public function getDescription(){
        return $this->_getData(self::DATA_DESCRIPTION);

    }

    /**
     * @return string|null
     */
    public function getContent(){
        return $this->_getData(self::DATA_CONTENT);
    }
    /**
     * @return string
     */
    public function getUrlRewrite(){
        return $this->_getData(self::DATA_URL_REWRITE);

    }
    /**
     * @return int
     */
    public function getStatus(){
        return $this->_getData(self::DATA_STATUS);

    }
    /**
     * @return string
     */
    public function getCreateAt(){
        return $this->_getData(self::DATA_CREATE_AT);
    }
    /**
     * @return string
     */
    public function getUpdateAt(){
        return $this->_getData(self::DATA_UPDATE_AT);

    }
    /**
     * @return int
     */
    public function getAuthorId(){
        return $this->_getData(self::DATA_AUTHOR);
    }
    /**
     * @return int
     */
    public function setId( $id) : mixed
    {
        return $this->setData(self::DATA_ID,$id);

    }
    /**
     * @return string
     */
    public function setTitle($title) : mixed
    {
        return $this->setData(self::DATA_TITLE,$title);
    }
    /**
     * @return string
     */
    public function setDescription( $description) : mixed
    {
        return $this->setData(self::DATA_DESCRIPTION,$description);
    }

    /**
     * @return string|null
     */
    public function setContent(string $content) : mixed {
        return $this->setData(self::DATA_CONTENT,$content);

    }
    /**
     * @return string
     */
    public function setUrlRewrite(string $url) : mixed {
        return $this->setData(self::DATA_URL_REWRITE,$url);

    }
    /**
     * @return int
     */
    public function setStatus(int $status) : mixed
    {
        return $this->setData(self::DATA_STATUS,$status);

    }
    /**
     * @return $this
     */
    public function setCreateAt( $create_at) : mixed
    {
        return $this->setData(self::DATA_CREATE_AT,$create_at);

    }
    /**
     * @return $this
     */
    public function setUpdateAt( $update_at) : mixed
    {
        return $this->setData(self::DATA_UPDATE_AT,$update_at);

    }
    /**
     * @return int
     */
    public function setAuthorId( $author_id) : mixed
    {
        return $this->setData(self::DATA_AUTHOR,$author_id);

    }
}
