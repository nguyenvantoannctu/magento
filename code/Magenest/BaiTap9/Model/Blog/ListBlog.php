<?php

namespace Magenest\BaiTap9\Model\Blog;

class ListBlog extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix  = 'magenestblog';
    protected function _construct()
    {
        $this->_init('Magenest\BaiTap9\Model\Blog\ResourceModel\ListBlog');
    }
}
