<?php

namespace Magenest\BaiTap9\Model\Blog\ResourceModel\Collection;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Magenest\BaiTap9\Model\Blog\ListBlog', 'Magenest\BaiTap9\Model\Blog\ResourceModel\ListBlog');
    }
}
