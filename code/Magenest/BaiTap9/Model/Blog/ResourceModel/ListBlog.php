<?php

namespace Magenest\BaiTap9\Model\Blog\ResourceModel;

class ListBlog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_blog', 'id');
    }

}
