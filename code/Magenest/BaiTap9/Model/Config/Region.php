<?php declare(strict_types=1);

namespace Magenest\BaiTap9\Model\Config;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

class Region extends AbstractSource implements OptionSourceInterface, SourceInterface
{
    public function getAllOptions(): array
    {
        return [
            [
                'value' => 1,
                'label' => 'Mien Bac',
            ],
            [
                'value' => 2,
                'label' => 'Mien Trung',
            ],
            [
                'value' => 3,
                'label' => 'Mien Nam',
            ]
        ];
    }
}
