<?php

namespace Magenest\BaiTap9\Model\ResourceModel\Blog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magenest\BaiTap9\Model\ResourceModel\Blog;

class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Magenest\BaiTap9\Model\Blog::class,
            Blog::class);
    }
}
