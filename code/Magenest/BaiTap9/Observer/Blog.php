<?php

namespace Magenest\BaiTap9\Observer;

use Magento\Framework\Event\Observer;

class Blog implements \Magento\Framework\Event\ObserverInterface
{
    protected $resoureUserFactory;
    protected $modelUserFactory;
    public function __construct(
                                \Magento\User\Model\ResourceModel\UserFactory $_resoureUserFactory,
                                \Magento\User\Model\UserFactory $_modelUserFactory,
                                )
    {
        $this->resoureUserFactory = $_resoureUserFactory;
        $this->modelUserFactory = $_modelUserFactory;
    }

    public function execute(Observer $observer)
    {
        $id = $observer['data_object']['id'];
        $object = $observer->getData('data_object');

        $model = $this->modelUserFactory->create();
        $user = $this->resoureUserFactory->create()->load($model,$id);

        $object->setData('user', $model->getData());
        //$dataUser = $model->getData();
        $observer->setData('data_object', $object);

    }
}
