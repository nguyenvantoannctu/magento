<?php

namespace Magenest\BaiTap9\Observer;

use Magento\Framework\Event\Observer;
use function PHPUnit\Framework\throwException;

class CheckURL implements \Magento\Framework\Event\ObserverInterface
{
    protected $urlrewriteCollection;
    protected $_messageManager;
    protected $_resultRedirectFactory;


    public function __construct(
                            \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $_urlrewriteCollection,
                            \Magento\Framework\Message\ManagerInterface $messageManager,
                            \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,

    )

    {

        $this->_resultRedirectFactory = $resultRedirectFactory;
        $this->_messageManager = $messageManager;
        $this->urlrewriteCollection = $_urlrewriteCollection;
    }
    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $post = $observer->getData();
        $originalURL = $post['data_object']->getOrigData('url_rewrite');
        $urlCheck = $observer->getData('data_object')->getData('url_rewrite');
        $this->_dataSaveAllowed=true;





        $listURL = $this->urlrewriteCollection->create();
        foreach ($listURL as $items){
            if($items->getData('request_path') == $urlCheck){
                throw new \Exception(
                    'Duplicate URL.'
                );
            }
        }
    }
}
