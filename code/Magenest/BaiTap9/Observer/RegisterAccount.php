<?php

namespace Magenest\BaiTap9\Observer;

use Magento\Framework\Event\Observer;

class RegisterAccount implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(Observer $observer)
    {
        $data = $observer->getData('postData');
        $observer->setData('postData', $data);
    }
}
