<?php

namespace Magenest\BaiTap9\Observer;

use Magento\Framework\Event\Observer;
use function PHPUnit\Framework\throwException;

class RewriteURL implements \Magento\Framework\Event\ObserverInterface
{
    protected $urlRewriteModel;
    protected $_messageManager;
    protected $urlrewriteColelction;
    protected $cacheTypeList;
    protected $cacheFrontendPool;
    protected $_blogRepository;
    protected $_blogFactory;

    public function __construct(
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory $_urlrewriteCollection,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $_urlRewriteModel,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magenest\BaiTap9\Api\BlogRepositoryInterface $blogRepository,
        \Magenest\BaiTap9\Model\BlogFactory $blogFactory,



    )
    {
        $this->_blogFactory = $blogFactory;
        $this->_blogRepository = $blogRepository;

        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
        $this->urlrewriteColelction = $_urlrewriteCollection;
        $this->_messageManager = $messageManager;
        $this->urlRewriteModel = $_urlRewriteModel;
    }
    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        // Get data of Blog
        $post = $observer->getData();
        $blogID = $post['data_object']['id'];
        $url = 'frontend/post/details/id/' . $blogID;

        $request_path = $post['data_object']['url_rewrite'];
        $entity_type = 'custom';
        $entity_id = 0;
        $store_id = 1;
        $status = true;

        $url_rewriteCollection = $this->urlrewriteColelction->create();
        foreach ($url_rewriteCollection as $items){
            $avc = $items->getTargetPath();
            if($items->getTargetPath() == $url){
                $model = $this->urlRewriteModel->create()->load($items->getID(),'url_rewrite_id');
                $model->setData('request_path',$request_path);
                $model->save();
                $status = false;
            }
        }
        if($status){
            $model = $this->urlRewriteModel->create();
            $model ->addData([
                "entity_type" => $entity_type,
                "entity_id" => $entity_id,
                "request_path" =>  $request_path,
                "target_path" => $url,
                "redirect_type" => 0,
                "store_id" => 1,
            ]);
            $model->save();
        }
        $blogTest = $this->_blogRepository->getById(2);
        $blog = $this->_blogFactory->create();
        $blog->setTitle('Olala');
        $blog->setUrlRewrite('kekeke');
        $this->_blogRepository->save($blog);




        $this->ClearCache();

    }
    public function ClearCache()
    {
//        $types = array('config','layout','block_html','collections','reflection','db_ddl','eav','config_integration','config_integration_api','full_page','translate','config_webservice');
//        $types = array('block_html');
//        foreach ($types as $type) {
//            $this->cacheTypeList->cleanType($type);
//        }
        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
