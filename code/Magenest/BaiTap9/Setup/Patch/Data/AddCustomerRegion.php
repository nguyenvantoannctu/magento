<?php declare(strict_types=1);

namespace Magenest\BaiTap9\Setup\Patch\Data;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Customer\Model\ResourceModel\Attribute as AttributeResourceModel;

use Zend_Validate_Exception;

class AddCustomerRegion implements DataPatchInterface
{
    const ATTRIBUTE_CODE = 'vn_region';

    private AttributeResourceModel $attributeResourceModel;
    private EavConfig $eavConfig;
    private EavSetupFactory $eavSetupFactory;
    private ModuleDataSetupInterface $moduleDataSetup;

    public function __construct(
        AttributeResourceModel $attributeResourceModel,
        EavConfig $eavConfig,
        EavSetupFactory $eavSetupFactory,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->attributeResourceModel = $attributeResourceModel;
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return $this
     * @throws LocalizedException
     * @throws AlreadyExistsException
     * @throws Zend_Validate_Exception
     */
    public function apply(): self
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->addAttribute(
            AddressMetadataInterface::ENTITY_TYPE_ADDRESS,
            self::ATTRIBUTE_CODE,
            [
                'type' => 'varchar',
                'label' => 'Region',
                'input' => 'select',
                'source' => \Magenest\BaiTap9\Model\Config\Region::class,
                'required' => false,
                'default' => 0,
                'system' => false,
                'position' => 150,
                'sort_order' => 150,
            ]
        );
        $attribute = $this->eavConfig->getAttribute(
            AddressMetadataInterface::ENTITY_TYPE_ADDRESS,
            self::ATTRIBUTE_CODE
        );
        $attribute->setData('used_in_forms', [
            'adminhtml_customer_address',
            'customer_address_edit',
            'customer_register_address',
            'customer_account_edit',
            'adminhtml_customer'
        ]);
        $this->attributeResourceModel->save($attribute);

        return $this;
    }
}
