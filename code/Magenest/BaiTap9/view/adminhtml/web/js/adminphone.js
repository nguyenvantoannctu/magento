require(
    [
        'Magento_Ui/js/lib/validation/validator',
        'jquery',
        'jquery/validate',
        'mage/translate'
    ], function(validator, $){

        validator.addRule(
            'validationphone',
            function(value, element) {
                //Perform your operation here and return the result true/false.
                return /((^(\+84|84|0|0084){1})(3|5|7|8|9))+([0-9]{8})$/.test(value);
            },
            $.mage.__("Your Phone must be 10 characters or Only number.")
        );
    });
