<?php

namespace Magenest\Chapter6\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Framework\Module\ModuleList;

class getLeftContent extends Template{

    protected $loader;
    protected $moduleManager;
    protected $_template = 'Magenest_Chapter6::menu/left.phtml';

    /**
     * @var array|int[]|string[]
     */

    public function __construct	(
        \Magento\Backend\Block\Template\Context $context,
        ModuleList\Loader 	$loader
    ){
        $this->loader = $loader;
//        $this->setTemplate('Magenest_Chapter6::menu/left.phtml');
        parent::__construct($context);



    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAll(): array
    {
        return $this->loader->load();
    }
}
