<?php

namespace Magenest\Chapter6\Block\Adminhtml;

use Magento\Backend\Block\Template;

class getLegend extends Template
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
    ) {
        parent::__construct($context);
    }
}
