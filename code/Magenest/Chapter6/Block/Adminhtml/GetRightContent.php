<?php

namespace Magenest\Chapter6\Block\Adminhtml;

use Magento\Backend\Block\Template;

class getRightContent extends Template
{
    protected $_productCollection;
    protected $_orderCollection;
    protected $_invoiceCollection;
    protected $_creditmemoCollection;
    protected $_customerCollection;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection,
        \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\Collection $invoiceCollection,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\Collection $creditmemoCollection,
        \Magento\Customer\Model\ResourceModel\Grid\Collection $customerCollection
    ) {
        $this->_productCollection = $productCollection;
        $this->_orderCollection = $orderCollection;
        $this->_invoiceCollection = $invoiceCollection;
        $this->_creditmemoCollection = $creditmemoCollection;
        $this->_customerCollection = $customerCollection;
        parent::__construct($context);
    }
    public function getProduct(): ?array
    {
        return $this->_productCollection->getData();
    }

    public function getOrder(): ?array
    {
        return $this->_orderCollection->getData();
    }
    public function getCustomer(): ?array
    {
        return $this->_customerCollection->getData();
    }
    public function getInvoice(): ?array
    {
        return $this->_invoiceCollection->getData();
    }
    public function getCreditmemo(): ?array
    {
        return $this->_creditmemoCollection->getData();
    }
}
