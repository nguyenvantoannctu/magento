<?php

namespace Magenest\Chapter6\Controller\Adminhtml\Movie;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class AddNew extends  Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New Movie'));
        return $resultPage;
    }
}
