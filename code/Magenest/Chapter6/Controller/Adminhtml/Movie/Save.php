<?php

namespace Magenest\Chapter6\Controller\Adminhtml\Movie;

use Magenest\Chapter6\Model\MovieFactory;
use Magento\Backend\App\Action;

/**
 * Class Save
\ */
class Save extends Action
{
    /**
     * @var MovieFactory
     */
    private $postFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param MovieFactory $postFactory
     */
    public function __construct(
        Action\Context $context,
        MovieFactory $postFactory
    ) {
        parent::__construct($context);
        $this->postFactory = $postFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['movie_id']) ? $data['movie_id'] : null;

        $newData = [
            'name' => $data['name'],
            'description' => $data['description'],
            'rating' => $data['rating'],
            'director_id' => $data['director_id']
        ];

        $post = $this->postFactory->create();

        if ($id) {
            $post->load($id);
            $this->getMessageManager()->addSuccessMessage(__('Edit thành công'));
        }else {
            $this->getMessageManager()->addSuccessMessage(__('Save thành công.'));
        }

        try {
            $post->addData($newData);
            $this->_eventManager->dispatch("adminhtml_movie_after_save",['postData'=>$post]);
            $post->save();
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('chapter6/movie/index');
    }
}
