<?php

namespace Magenest\Chapter6\Model\Config\Director;

use Magenest\Chapter6\Model\ResourceModel\Collection\DirectorCollection;

class DirectorOptions implements \Magento\Framework\Data\OptionSourceInterface
{
    protected $_DirectorCollectionFactory;
    protected $_options;
    public function __construct(\Magenest\Chapter6\Model\ResourceModel\Collection\DirectorCollectionFactory $DirectorCollectionFactory)
    {
        $this->_DirectorCollectionFactory = $DirectorCollectionFactory;
    }
    public function toOptionArray()
    {
        if($this->_options === null){
            $collection = $this->_DirectorCollectionFactory->create();

            $this->_options = [[ 'label' => '' , 'value' => '' ]];

            foreach ($collection as $director){
                $this->_options[] = [
                    'label' => _($director->getName()),
                    'value' => $director->getDirectorId()
                ];
            }
        }
        return $this->_options;
    // TODO: Implement toOptionArray() method.
    }
}
