<?php

namespace Magenest\Chapter6\Model\Config;
use Magento\Backend\Block\Template\Context;

class NumberOfCustomer extends  \Magento\Config\Block\System\Config\Form\Field
{
    protected $_customer;
    public function __construct(Context $context,
                                \Magento\Customer\Model\Customer $customers,
        $data = [])
    {
        $this->_customer = $customers;
        parent::__construct($context, $data);
    }
    public function getCollection(){
        return $this->_customer->getCollection()
            ->addAttributeToSelect("*")
            ->load();
    }
    public function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $customerCollection = $this->getCollection();
        $total = $customerCollection->count();
        $element->setData('value',$total);
        $element->setReadonly('true');
        return parent::_getElementHtml($element); // TODO: Change the autogenerated stub
    }
}
