<?php

namespace Magenest\Chapter6\Model\Config;
use Magento\Backend\Block\Template\Context;
use \Magento\Framework\Data\Form\Element\AbstractElement;

class NumberOfProduct extends  \Magento\Config\Block\System\Config\Form\Field
{
    protected $_productCollection;
    public function __construct(Context $context,
                                \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $_productCollection,
                                $data = [])
    {
        $this->_productCollection = $_productCollection;
        parent::__construct($context, $data);
    }
    public function _getElementHtml(AbstractElement $element)
    {
        $collection = $this->_productCollection->create();
        $element->setData('value',$collection->count());
        $element->setReadonly('true');
        return parent::_getElementHtml($element); // TODO: Change the autogenerated stub
    }
}
