<?php

namespace Magenest\Chapter6\Model;

class Director extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magenest\Chapter6\Model\ResourceModel\Director');
    }
}
