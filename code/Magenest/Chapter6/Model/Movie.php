<?php

namespace Magenest\Chapter6\Model;

class Movie extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magenest\Chapter6\Model\ResourceModel\Movie');
    }
}
