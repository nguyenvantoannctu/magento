<?php

namespace Magenest\Chapter6\Model\ResourceModel\Collection;

class DirectorCollection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'director_id';

    protected function _construct()
    {
        $this->_init('Magenest\Chapter6\Model\Director', 'Magenest\Chapter6\Model\ResourceModel\Director');
    }
}
