<?php

namespace Magenest\Chapter6\Model\ResourceModel\Collection;

class MovieCollection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'movie_id';

    protected function _construct()
    {
        $this->_init('Magenest\Chapter6\Model\Movie', 'Magenest\Chapter6\Model\ResourceModel\Movie');
    }
}
