<?php

namespace Magenest\Chapter6\Model\ResourceModel;

class Director extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_director', 'director_id');
    }
}
