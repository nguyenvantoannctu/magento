<?php

namespace Magenest\Chapter6\Ui\Component\Listing\Grid\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class UpdateRatingStar extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if(isset($dataSource['data']['items'])){

            foreach ($dataSource['data']['items'] as& $item){
                if($item['rating'] == 1){
                    $item['rating'] = "&#9734;";
                }
                else if($item['rating'] == 2){
                    $item['rating'] = "&#9734; &#9734;";
                }
                else if($item['rating'] == 3){
                    $item['rating'] = "&#9734; &#9734; &#9734;";
                }
                else if($item['rating'] == 4){
                    $item['rating'] = "&#9734; &#9734; &#9734; &#9734;";
                }
                else if($item['rating'] == 5){
                    $item['rating'] = "&#9734; &#9734; &#9734; &#9734; &#9734;";
                }
                else{
                    $item['rating'] = "Not have review yet";
                }
            }

        }
        return $dataSource;
    }
}
