<?php

namespace Magenest\Chapter7\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;


class ConfigChange implements ObserverInterface
{
    private $request;
    private $configWriter;

    /**
     * ConfigChange constructor.
     * @param RequestInterface $request
     * @param WriterInterface $configWriter
     */
    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter
    )
    {
        $this->request = $request;
        $this->configWriter = $configWriter;
    }

    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $faqParams = $this->request->getParam('groups');

        $value = $faqParams['group_magenest']['fields']['txt_field']['value'];

        if($value == 'Ping'){
            $value = 'Pong';
        }
        $this->configWriter->save('section_magenest/group_magenest/txt_field',$value);
        return $this;

    }
}
