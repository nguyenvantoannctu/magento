<?php

namespace Magenest\Chapter7\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerName implements ObserverInterface
{
    protected $_customerRepository;

    public function __construct(
        CustomerRepositoryInterface $_customerRepository
    )
    {
        $this->_customerRepository = $_customerRepository;
    }

    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $customer = $observer->getEvent()->getCustomer();
        $customer->setData('firstname','Magenest');

        $this->_customerRepository->save($customer);

        return $this;


    }
}
