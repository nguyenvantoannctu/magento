<?php

namespace Magenest\Chapter7\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class MovieRating implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $data = $observer->getData('postData');

        $data->setData('rating', 0);
        $observer->setData('postData', $data);
    }
}
