<?php

namespace Magenest\Chapter7\Plugin;

class Add {
    private $request;
    private $productRepository;
    private $configProduct;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $product,
        \Magento\Catalog\Model\ProductRepository $productRepository
    )
    {
        $this->configProduct = $product;
        $this->productRepository = $productRepository;
        $this->request = $request;
    }
    public function beforeExecute(\Magento\Checkout\Controller\Cart\Add $subject)
    {
        $configProduct = $this->productRepository->getById((int)$this->request->getParam('product'));
        $post = $this->request->getPostValue();
        if(array_key_exists('super_attribute', $post)) {
            $childConfigProduct = $this->configProduct->getProductByAttributes($post['super_attribute'], $configProduct);
            $this->request->setParams(array('product' => $childConfigProduct->getEntityId()));
        }
        else $this->request->setParams(array('product' => $configProduct->getEntityId()));
        return null;
    }
}
