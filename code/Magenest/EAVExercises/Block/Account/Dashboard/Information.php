<?php
namespace Magenest\EAVExercises\Block\Account\Dashboard;

use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Customer\Helper\View;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Information extends Template
{

    /**
     * Constructor
     * @param Context $context
     * @param CurrentCustomer $currentCustomer
     * @param View $helperView
     * @param array $data
     */
    protected $currentCustomer;
    protected $helperView;
    protected $storeManager;
    protected $customerModel;
    protected $customerSession;

    public function __construct(
        Context $context,
        CurrentCustomer $currentCustomer,
        View $helperView,
        SessionFactory $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Customer $customerModel,
        UrlInterface $urlBuilder,
        array $data = []
    ) {
        $this->urlBuilder                  = $urlBuilder;
        $this->customerSession          = $customerSession->create();
        $this->storeManager               = $storeManager;
        $this->customerModel             = $customerModel;
        $this->helperView = $helperView;
        $this->currentCustomer = $currentCustomer;
        parent::__construct($context, $data);
    }
//
//    /**
//     * @inheritdoc
//     */
//    protected function _toHtml()
//    {
//        return $this->currentCustomer->getCustomerId() ? parent::_toHtml() : '';
//    }
    /**
     * get Customer
     */
    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }
    /**
     * get Name Customer
     */
    public function getName()
    {
        return $this->helperView->getCustomerName($this->getCustomer());
    }

    /**
     * @inheritdoc
     */
    protected function _toHtml()
    {
        return $this->currentCustomer->getCustomerId() ? parent::_toHtml() : '';
    }

    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl();
    }

    public function getMediaUrl()
    {
        return $this->getBaseUrl() . 'pub/media/';
    }

    public function getCustomerImageUrl($filePath)
    {
        return $this->getMediaUrl() . 'customer' . $filePath;
    }

    public function getFileUrl()
    {
        $customerData = $this->customerModel->load($this->customerSession->getId());
        $url = $customerData->getData('avatar');
        if (!empty($url)) {
            return $this->getCustomerImageUrl($url);
        }
        return false;
    }
}
