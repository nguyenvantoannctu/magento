define([
    'jquery',
    'Magento_Ui/js/modal/alert'
], function ($,alert) {
    'use strict';
    $('#btn-test').on('click', function(){
        alert({
            title: 'Alert Title',
            modalClass: 'alert',
            actions: {
                always: function() {
                    // do something when the modal is closed
                }
            },
            buttons: [{
                text: $.mage.__('Accept'),
                class: 'action primary accept',

                /**
                 * Click handler.
                 */
                click: function () {
                    this.closeModal(true);
                }

            }],
        });
    })
});
