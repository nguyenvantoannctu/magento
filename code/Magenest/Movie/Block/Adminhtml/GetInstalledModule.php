<?php

namespace Magenest\Movie\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Framework\Module\ModuleList;


class GetInstalledModule extends Template{

    protected $loader;
    protected $moduleManager;


    /**
     * @var array|int[]|string[]
     */

    public function __construct	(
                                    \Magento\Backend\Block\Template\Context $context,
                                    ModuleList\Loader 	$loader
                                ){
        $this->loader = $loader;
        parent::__construct($context);



    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAll(): array
    {
       return $this->loader->load();
    }
}


