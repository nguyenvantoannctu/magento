<?php

namespace Magenest\Movie\Block;

class Color extends \Magento\Framework\View\Element\Template
{
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return '<input type="color" id="' .
            $element->getHtmlId() .
            '" name="' .
            $element->getName() .
            '" value="' .
            $element->getEscapedValue() .
            '"/>';
    }
}
