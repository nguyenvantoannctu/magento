<?php

namespace Magenest\Movie\Block\Movie;
use Magento\Framework\View\Element\Template;


class GetAllActor extends \Magento\Framework\View\Element\Template
{
    protected $ListActorFactoryCollection;

    public function __construct(Template\Context $context,
                                \Magenest\Movie\Model\ResourceModel\Collection\ActorCollection $postFactory,
                                array $data = []
    )
    {
        $this->ListActorFactoryCollection = $postFactory;

        parent::__construct($context, $data);

    }

    public function getActor()
    {
        $collection  = $this->ListActorFactoryCollection->getData();
        return $collection;
    }

}

