<?php

namespace Magenest\Movie\Block\Movie;
use Magento\Framework\View\Element\Template;


class GetAllMovie extends \Magento\Framework\View\Element\Template
{
    protected $ListMovieFactoryCollection;

    public function __construct(Template\Context $context,
                                \Magenest\Movie\Model\ResourceModel\Collection\MovieCollectionFactory $postFactory,
                                array $data = []
    )
    {
        $this->ListMovieFactoryCollection = $postFactory;

        parent::__construct($context, $data);

    }

    public function getMovie()
    {
        $collection  = $this->ListMovieFactoryCollection->create();
        $data = $collection->joinTable();
        return $collection;
    }
    public function getRatingSummary($product)
    {
        $this->_reviewFactory->create()->getEntitySummary($product, $this->_storeManager->getStore()->getId());
        $ratingSummary = $product->getRatingSummary()->getRatingSummary();
        return $ratingSummary;
    }


}

