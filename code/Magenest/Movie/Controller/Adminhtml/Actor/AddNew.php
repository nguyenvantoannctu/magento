<?php

namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

/**
 * Class AddNew
 * @package Magenest\Movie\Controller\Adminhtml\Actor
 */
class AddNew extends Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New Actor'));
        return $resultPage;
    }
}
