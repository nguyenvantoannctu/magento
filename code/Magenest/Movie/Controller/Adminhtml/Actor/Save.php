<?php

namespace Magenest\Movie\Controller\Adminhtml\Actor;

use Magenest\Movie\Model\ListActorFactory;
use Magento\Backend\App\Action;

/**
 * Class Save
 * @package Magenest\Movie\Controller\Adminhtml\Director
 */
class Save extends Action
{
    /**
     * @var ListActorFactory
     */
    private $postFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param ListActorFactory $postFactory
     */
    public function __construct(
        Action\Context $context,
        ListActorFactory $postFactory
    ) {
        parent::__construct($context);
        $this->postFactory = $postFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['actor_id']) ? $data['actor_id'] : null;

        $newData = [
            'name' => $data['name'],
        ];

        $post = $this->postFactory->create();

        if ($id) {
            $post->load($id);
        }
        try {
            $post->addData($newData);
            $post->save();
            $this->messageManager->addSuccessMessage(__('You saved the actor.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('backend/actor/index');
    }
}
