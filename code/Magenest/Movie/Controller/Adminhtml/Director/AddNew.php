<?php

namespace Magenest\Movie\Controller\Adminhtml\Director;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

/**
 * Class AddNew
 * @package Magenest\Movie\Controller\Adminhtml\Director
 */
class AddNew extends Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New Director'));
        return $resultPage;
    }
}
