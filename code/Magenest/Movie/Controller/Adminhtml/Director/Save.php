<?php

namespace Magenest\Movie\Controller\Adminhtml\Director;

use Magenest\Movie\Model\ListDirectorFactory;
use Magento\Backend\App\Action;

/**
 * Class Save
 * @package Magenest\Movie\Controller\Adminhtml\Director
 */
class Save extends Action
{
    /**
     * @var ListDirectorFactory
     */
    private $postFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param ListDirectorFactory $postFactory
     */
    public function __construct(
        Action\Context $context,
        ListDirectorFactory $postFactory
    ) {
        parent::__construct($context);
        $this->postFactory = $postFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['director_id']) ? $data['director_id'] : null;

        $newData = [
            'name' => $data['name'],
        ];

        $post = $this->postFactory->create();

        if ($id) {
            $post->load($id);
        }
        try {
            $post->addData($newData);
            $post->save();
            $this->messageManager->addSuccessMessage(__('You saved the director.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('backend/director/index');
    }
}
