<?php

namespace Magenest\Movie\Controller\Adminhtml\Export;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;


class Exportdata extends \Magento\Backend\App\Action{

    protected $uploaderFactory;
    protected $_locationFactory;
    protected $_fileFactory;
    protected $_orderFactory;
    protected $_redirectory;
    public function __construct(Context $context,
                                \Magento\Framework\App\Response\Http\fileFactory $fileFactory,
                                \Magento\Framework\Filesystem $filesystem,
                                \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection,
                                )
    {
        $this->_fileFactory = $fileFactory;
        $this->_orderFactory = $orderCollection;
        $this->_redirectory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);

    }
    public function execute()
    {
        // TODO: Implement execute() method.
        $name = date('m-d-Y-H-i-s');
        $filepath = 'export/eport-data' . $name . '.csv';
        $this->_redirectory->create('export');

        $stream = $this->_redirectory->openFile($filepath, 'w+');
        $stream->lock();

        $columns = ['ID Order','Purcharse Point','Purcharse Date','Date','Ref Code','Product Name','Product ???','Qty','Unit Price', 'Grand Total (Base)'];

        foreach ($columns as $column){
            $header[] = $column;
        }

        $stream->writeCsv($header);

        $order = $this->_orderFactory->getData();

        foreach ($order as $item){

            $itemData = [];

            $itemData[] = $item->getData('entity_id');
            $itemData[] = $item->getData('store_name');
            $itemData[] = $item->getData('created_at');
            $itemData[] = $item->getData('product_type');
            $itemData[] = $item->getData('protect_code');
            $itemData[] = $item->getData('name');
            $itemData[] = $item->getData('sku');
            $itemData[] = $item->getData('qty_ordered');
            $itemData[] = $item->getData('price');
            $itemData[] = $item->getData('base_grand_total');

            $stream->writeCsv($itemData);

        }
        $content = [];
        $content['type'] = 'filename'; /// must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; // remove csv from var folder

        $csvfilename = 'locator-import'.$name.'.csv';
        return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);

    }
}
