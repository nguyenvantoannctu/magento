<?php

namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

/**
 * Class AddNew
 * @package Magenest\Movie\Controller\Adminhtml\Movie
 */
class AddNew extends Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New Movie'));
        return $resultPage;
    }
}
