<?php

namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magenest\Movie\Model\ListMovieFactory;
use Magento\Backend\App\Action;

/**
 * Class Save
 * @package Magenest\Movie\Controller\Adminhtml\Movie
 */
class Save extends Action
{
    /**
     * @var ListMovieFactory
     */
    private $postFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param ListMovieFactory $postFactory
     */
    public function __construct(
        Action\Context $context,
        ListMovieFactory $postFactory
    ) {
        parent::__construct($context);
        $this->postFactory = $postFactory;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['movie_id']) ? $data['movie_id'] : null;

        $newData = [
            'name' => $data['name'],
            'description' => $data['description'],
            'rating' => $data['rating'],
            'director_id' => $data['director_id']
        ];

        $post = $this->postFactory->create();

        if ($id) {
            $post->load($id);
            $this->getMessageManager()->addSuccessMessage(__('Edit thành công'));
        }else {
            $this->getMessageManager()->addSuccessMessage(__('Save thành công.'));
        }

        try {
            $post->addData($newData);
            $this->_eventManager->dispatch("admintml_movie_aftersave",['postData'=>$post]);
            $post->save();
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setPath('backend/movie/index');
    }
}
