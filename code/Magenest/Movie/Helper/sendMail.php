<?php
namespace Magenest\Movie\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;



class sendMail extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_EMAIL_TEMPLATE_FIELD  = 'smtp_faa/group_smtp/Mail_Template';

    protected $inlineTranslation;
    protected $escaper;
    protected $transportBuilder;
    protected $logger;
    protected $_storeManager;
    protected $_dynamicCollection;



    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,

        \Magenest\Movie\Model\ResourceModel\Collection\DynamicCollection $dynamicCollection

    ) {
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $context->getLogger();
        $this->_storeManager = $storeManager;
        $this->_dynamicCollection = $dynamicCollection;
        parent::__construct($context);
    }

    public function prepareTemplate(Order $order)
    {
        $templateId = $this->getTemplateId(self::XML_PATH_EMAIL_TEMPLATE_FIELD);
        $customerEmail = $order->getCustomerEmail();
        $store = $this->_storeManager->getStore();
        $order->getShippingDescription();

        $fromEmail = 'nguyenvantoannctu@gmai.com';

        $fromName = 'ADMIN';
        $toEmail = 'bhanhtai0101@gmail.com';

        try{
            $templateVars = [
                'order' => $order,
                'store'=> $store,
            ];

            $storeId = $this->_storeManager->getStore()->getId();
            $from = ['email'=>$fromEmail, 'name' => $fromName];
            $this->inlineTranslation->suspend();

            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store'=> $storeId,
            ];
            $transport = $this->transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($customerEmail)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        }
        catch(\Exception $e){
            $this->_logger->info($e->getMessage());
        }

    }
    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
    public function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }
}
