<?php

namespace Magenest\Movie\Model\Config;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Disabled extends  \Magento\Config\Block\System\Config\Form\Field
{
    protected $ListMovieFactoryCollection;
    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                \Magenest\Movie\Model\ResourceModel\Collection\MovieCollectionFactory $postFactory,
                                array $data = []
    )
    {
        $this->ListMovieFactoryCollection = $postFactory;

        parent::__construct($context);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $collection = $this->ListMovieFactoryCollection->create();
        $element->setData('value', $collection->count());

        $element->setReadonly('true');
        return $element->getElementHtml();
    }
}
