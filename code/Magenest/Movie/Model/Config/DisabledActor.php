<?php

namespace Magenest\Movie\Model\Config;
use Magento\Framework\Data\Form\Element\AbstractElement;

class DisabledActor extends  \Magento\Config\Block\System\Config\Form\Field
{
    protected $ListActorCollection;
    public function __construct(\Magento\Backend\Block\Template\Context $context,
                                \Magenest\Movie\Model\ResourceModel\Collection\ActorCollectionFactory $postFactory,
                                array $data = []
    )
    {
        $this->ListActorCollection = $postFactory;

        parent::__construct($context);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $collection = $this->ListActorCollection->create();
        $element->setData('value', $collection->count());

        $element->setReadonly('true');
        return $element->getElementHtml();
    }
}
