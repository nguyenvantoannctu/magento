<?php

namespace Magenest\Movie\Model\Config\Email;

class GetTemplateEmail implements \Magento\Framework\Data\OptionSourceInterface
{
    protected $_templateCollection;
    protected $_options;


    public function __construct(
        \Magento\Email\Model\ResourceModel\Template\CollectionFactory $templateCollection
    )
    {
        $this->_templateCollection = $templateCollection;
    }
    public function toOptionArray() : array
    {
        // TODO: Implement toOptionArray() method.
        return $this->_templateCollection->create()->toOptionArray();
    }
}
