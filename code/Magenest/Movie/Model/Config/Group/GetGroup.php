<?php
namespace Magenest\Movie\Model\Config\Customer\Group;

class GetGroup implements \Magento\Framework\Data\OptionSourceInterface
{
    protected $_customerGroupCollectionFactory;

    protected $_options;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroupCollectionFactory
    )
    {
        $this->_customerGroupCollectionFactory = $customerGroupCollectionFactory;
    }

    public function toOptionArray() : array
    {
        // TODO: Implement toOptionArray() method.
        if($this->_options === null){
            $collection = $this->_customerGroupCollectionFactory;

            $this->_options = [[ 'label' => '' , 'value' => '' ]];

            foreach ($collection as $group){
                $this->_options[] = [
                    'label' => _($group->getCustomerGroupCode()),
                    'value' => $group->getCustomerGroupId()
                ];
            }
        }
        return $this->_options;
    }
}
