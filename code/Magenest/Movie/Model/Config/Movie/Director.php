<?php
namespace Magenest\Movie\Model\Config\Movie;

class Director implements \Magento\Framework\Data\OptionSourceInterface
{
    protected $_DirectorCollectionFactory;

    protected $_options;

    public function __construct(
        \Magenest\Movie\Model\ResourceModel\Collection\DirectorCollectionFactory $DirectorCollectionFactory
    )
    {
        $this->_DirectorCollectionFactory = $DirectorCollectionFactory;
    }

    public function toOptionArray() : array
    {
        // TODO: Implement toOptionArray() method.
        if($this->_options === null){
            $collection = $this->_DirectorCollectionFactory->create();

            $this->_options = [[ 'label' => '' , 'value' => '' ]];

            foreach ($collection as $director){
                $this->_options[] = [
                    'label' => _($director->getName()),
                    'value' => $director->getID()
                ];
            }
        }
        return $this->_options;
    }
}
