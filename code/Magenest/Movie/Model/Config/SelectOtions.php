<?php

namespace Magenest\Movie\Model\Config;

use Magento\Framework\Data\OptionSourceInterface;

class SelectOtions implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray() : array
    {
        return [
          '1' => 'Show',
            '2' => 'Not Show'
        ];
    }

}
