<?php

namespace Magenest\Movie\Model;

class ListDynamic extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\ResourceModel\ListDynamic');
    }
    protected function getValueByID($id){
        if($this->getEntityId() == $id){
            return $this->getValue();
        }
    }

}
