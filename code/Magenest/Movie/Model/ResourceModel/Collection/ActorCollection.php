<?php

namespace Magenest\Movie\Model\ResourceModel\Collection;

class ActorCollection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'actor_id';

    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\ListActor', 'Magenest\Movie\Model\ResourceModel\ListActor');
    }

}
