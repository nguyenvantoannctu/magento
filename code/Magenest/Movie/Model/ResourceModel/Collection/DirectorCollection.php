<?php

namespace Magenest\Movie\Model\ResourceModel\Collection;

class DirectorCollection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'director_id';

    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\ListDirector', 'Magenest\Movie\Model\ResourceModel\ListDirector');
    }

}
