<?php

namespace Magenest\Movie\Model\ResourceModel\Collection;

class DynamicCollection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'dynamic_id';

    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\ListDynamic', 'Magenest\Movie\Model\ResourceModel\ListDynamic');
    }
    public function joinTable(){
        $tableProduct = $this->getTable('catalog_product_entity');
        $result = $this
            ->addFieldToSelect('entity_id','ID ')
            ->addFieldToSelect('type_id')
            ->addFieldToSelect('sku')
            ->join($tableProduct,'main_table.entity_id='.$tableProduct.'.entity_id',['director' => 'sku']);
        return $result;
    }

}
