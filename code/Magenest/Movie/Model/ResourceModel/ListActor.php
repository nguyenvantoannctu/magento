<?php

namespace Magenest\Movie\Model\ResourceModel;

class ListActor extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_actor', 'actor_id');
    }
}
