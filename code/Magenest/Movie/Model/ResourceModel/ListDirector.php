<?php

namespace Magenest\Movie\Model\ResourceModel;

class ListDirector extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_director', 'director_id');
    }
}
