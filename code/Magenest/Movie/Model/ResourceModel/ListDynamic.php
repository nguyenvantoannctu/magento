<?php

namespace Magenest\Movie\Model\ResourceModel;

class ListDynamic extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('dynamicrow', 'dynamic_id');
    }
}
