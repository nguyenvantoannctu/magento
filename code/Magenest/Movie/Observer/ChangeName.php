<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\ObserverInterface;

use Magento\Customer\Api\CustomerRepositoryInterface;

class ChangeName implements ObserverInterface

{

    protected $customerRepository;

    public function __construct(

        CustomerRepositoryInterface $customerRepository)

    {

        $this->customerRepository = $customerRepository;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)

    {

        $customer = $observer->getEvent()->getCustomer();

        $customer->setData('firstname', 'Magenest');

        $this->customerRepository->save($customer);

        return $this;

    }

}
