<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class ChangeTextField implements ObserverInterface
{

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * ConfigChange constructor.
     * @param RequestInterface $request
     * @param WriterInterface $configWriter
     */
    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter
    ) {
        $this->request = $request;
        $this->configWriter = $configWriter;

    }
    ///  Scope Config
    public function execute(EventObserver $observer)
    {
        $faqParams = $this->request->getParam('groups');
                            // Group      default    name_field
        $urlKey = $faqParams['group_id']['fields']['txt_field']['value']; //Current faq_url value, Here you can filter current value
        if($urlKey == 'Ping'){
            $urlKey = 'Pong';
        }                              //section // group /// field
        $this->configWriter->save('faa/group_id/txt_field', $urlKey);
        return $this;
    }
}
