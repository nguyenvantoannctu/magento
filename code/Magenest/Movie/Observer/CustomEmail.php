<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;
use Magenest\Movie\Helper\sendMail as Email;
use Magento\Framework\Event\ObserverInterface;

class CustomEmail implements ObserverInterface
{
    private $helperEmail;

    public function __construct(
                        Email $helperEmail
                            )
    {
        $this->helperEmail = $helperEmail;
    }
    public function execute(Observer $observer)
    {
        // TODO: Implement execute() metho
        $observer->getData('postData');
        $order = $observer['order'];


        $this->helperEmail->prepareTemplate($order);
    }
}
