<?php

namespace Magenest\Movie\Observer;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use \Magento\Framework\Serialize\Serializer\Json;


class DynamicRows implements ObserverInterface
{

    protected $productRepository;
    protected $request;
    protected $_json;
    protected $_dynamicCollection;
    protected $_listDynamicFactory;
    protected $_listDynamic;



    public function __construct(
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Magento\Framework\App\RequestInterface $request,
        ProductRepositoryInterface $_productRepository,
        \Magenest\Movie\Model\ResourceModel\Collection\DynamicCollectionFactory $dynamicCollectionFactory,
        \Magenest\Movie\Model\ListDynamicFactory $listDynamicFactory,
        \Magenest\Movie\Model\ResourceModel\ListDynamic $listDynamic
        )

    {
        $this->request = $request;
        $this->_json = $json;
        $this->productRepository = $_productRepository;
        $this->_dynamicCollection = $dynamicCollectionFactory;
        $this->_listDynamicFactory = $listDynamicFactory;
        $this->_listDynamic = $listDynamic;

    }
    ///  Scope Config
    public function execute(EventObserver $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $id_product = $product->getEntityId();

        $post = $this->request->getPost();
        $post = $post['product'];

        $dynamicData = $post['custom_fieldset'];

//        $dynamicRow = $this->_dynamicCollection->create();

        $dynamicRow = $this->_listDynamicFactory->create();
        $dynamicRow->setData(['dynamic_id'=>'4','entity_id'=>$id_product,'value'=>$this->_json->serialize($dynamicData)]);
//        $product->setTestdynamic($this->_json->serialize($dynamicData));
//        $product->setName($this->_json->serialize($dynamicData));
        $this->_listDynamic->save($dynamicRow);
//        $abc =$dynamicRow->getData();
//        $listRows = $this->_listDynamic->load($dynamicRow, $id_product, 'entity_id');

//        $txt = $observer->getData('data_object')->getData('custom_fieldset')['custom_field']['0']['textField'];
//
//        $product->setData('dynamic_product', $txt);
//
//        return $this;
    }

}
