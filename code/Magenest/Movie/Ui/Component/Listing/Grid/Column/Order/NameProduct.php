<?php
namespace Magenest\Movie\Ui\Component\Listing\Grid\Column\Order;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Sales\Api\OrderRepositoryInterface;


class NameProduct extends Column
{
    /**
     *
     * @p
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array              $components
     * @param array              $data
     */
    protected $_orderRepository;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        array $data = [],

    ) {
        $this->_orderRepository = $orderRepository;

        parent::__construct($context, $uiComponentFactory, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $items) {
                $productArr = [];
                $order = $this->_orderRepository->get($items["entity_id"]);

                foreach ($order->getAllVisibleItems() as $item) {
                    $productArr[] = $item->getSku();
                    $item->getSize();
                }
                $items['nameproduct_column'] = implode('<br>', $productArr);
            }
            unset($productArr);
            }
        return $dataSource;
    }
}
