<?php
namespace Magenest\Movie\Ui\Component\Listing\Grid\Column\Order;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class OddEven extends Column
{
    /**
     *
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
               if ($item['entity_id'] % 2 == 0){
                   $item['status_column'] = '<span class="grid-severity-notice"><span>Success</span></span>';
               }
               else{
                   $item['status_column'] = '<span class="grid-severity-critical"><span>Fail</span></span> ';
               }//Value which you want to display
            }
        }
        return $dataSource;
    }
}
