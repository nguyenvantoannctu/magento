<?php

namespace Magenest\Movie\Ui\DataProvider\Product\Form\Modifier;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Framework\UrlInterface;

/**
 * Data provider for "Custom Attribute" field of product page
 */
class Datetime extends AbstractModifier
{
    protected $locator;

    /**
     * @param ArrayManager                $arrayManager
     */
    public function __construct(
        ArrayManager $arrayManager,
        LocatorInterface $locator,
        UrlInterface $urlBuilder

    ) {
        $this->arrayManager = $arrayManager;
        $this->locator = $locator;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {

        if($this->locator->getProduct()->getTypeId() == 'virtual'){
            $meta = $this->enableTime($meta);
        }
        else{
            $meta = $this->disabled($meta);
        }
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Customise Custom Attribute field
     *
     * @param array $meta
     *
     * @return array
     */
    protected function enableTime(array $meta)
    {
        $fieldCode = 'datestart';

        $elementPath = $this->arrayManager->findPath($fieldCode, $meta, null, 'children');

        $containerPath = $this->arrayManager->findPath(static::CONTAINER_PREFIX . $fieldCode, $meta, null, 'children');

        if (!$elementPath) {
            return $meta;
        }

        $meta = $this->arrayManager->merge(
            $containerPath,
            $meta,
            [
                'children'  => [
                    $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'component' => 'Magenest_Movie/js/dateinput',
                                    'options'       => [
                                        'dateFormat' > 'dd-mm-Y',
                                        'timeFormat' => 'HH:mm:ss',
                                        'showsTime' => true,
                                    ]

                                ],
                            ],
                        ],
                    ],
                ]
            ]
        );


        return $meta;
    }
    protected function disabled(array $meta)
    {
        $fieldCode = 'datestart';

        $elementPath = $this->arrayManager->findPath($fieldCode, $meta, null, 'children');

        $containerPath = $this->arrayManager->findPath(static::CONTAINER_PREFIX . $fieldCode, $meta, null, 'children');

        if (!$elementPath) {
            return $meta;
        }
        $meta = $this->arrayManager->merge(
            $containerPath,
            $meta,
            [
                'children'  => [
                    $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'visible' => false,
                                    'disabled'=> true,
                                ],
                            ],
                        ],
                    ]
                ]
            ]
        );


        return $meta;
    }
}
