<?php
namespace Magenest\Movie\Ui\DataProvider\Product\Form\Modifier;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\ActionDelete;
use Magento\Ui\Component\Form\Element\DataType\Text;
class DynamicRow extends AbstractModifier
{
    const FIELD_IS_DELETE = 'is_delete';
    const FIELD_SORT_ORDER_NAME = 'sort_order';
    const FIELD_NAME_TEXT = 'text_field';

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var array
     */
    protected $scopeName;

    /**
     * @param LocatorInterface $locator
     * @param ArrayManager $arrayManager
     */
    protected $_dymanicCollectionFactory;
    protected $_json;
    const DYNAMIC_ROW_CODE = 'dynamicrow';
    const FIELD_NAME = 'custom_fieldset';

    private $locator;
    public function __construct(
        LocatorInterface $locator,
        ArrayManager $arrayManager,
        \Magenest\Movie\Model\ResourceModel\Collection\DynamicCollectionFactory $dynamicCollectionFactory,
        \Magento\Framework\Serialize\Serializer\Json $json,

    ) {
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
        $this->_dymanicCollectionFactory = $dynamicCollectionFactory;
        $this->_json = $json;

    }
    public function modifyData(array $data)
    {
        $filedCode = self::DYNAMIC_ROW_CODE;
//
        $listDynamic = $this->_dymanicCollectionFactory->create();
        $DynamicData = $listDynamic->getData('value');
        $model = $this->locator->getProduct();
        $productId = $this->locator->getProduct()->getId();
        $modelId = $model->getId();


//        $data[$productId][self::DATA_SOURCE_DEFAULT][self::FIELD_NAME] = $this->_json->unserialize($DynamicData['0']['value']);


        $path = $modelId . '/' . self::DATA_SOURCE_DEFAULT . '/' . self::FIELD_NAME;
        $fieldData = $this->_json->unserialize($DynamicData['0']['value']);
        $data = $this->arrayManager->set($path,$data,$fieldData);


//        $fieldData = $this->_json->unserialize($DynamicData['0']['entity_id']);
//        $data = $this->arrayManager->set($modelId,);
//
//        $listFiledData = $this->_dymanicCollectionFactory->create()->getData();
//        if($listFiledData){
//            foreach ($listFiledData as $items){
//                $filedData = $this->_json->unserialize(($items)($items, true));
//                $path = $modelId . '/' . self::DATA_SOURCE_DEFAULT . '/' . self::DYNAMIC_ROW_CODE;
//                $data = $this->arrayManager->set($path, $data, $filedData);
//            }
//
//        }
        return $data;
    }
    public function modifyMeta(array $meta)
    {
        $meta = array_replace_recursive(
            $meta,
            [
                'custom_fieldset' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Custom Group'),
                                'componentType' => Fieldset::NAME,
                                'dataScope' => 'data.product.custom_fieldset',
                                'collapsible' => true,
                                'sortOrder' => 5,
                            ],
                        ],
                    ],
                    'children' => [
                        "custom_field" => $this->getTextGridConfig(10)
                    ],
                ]
            ]
        );
        return $meta;
    }
    protected function getTextGridConfig($sortOrder) {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add Value'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Ui/js/dynamic-rows/dynamic-rows',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'renderDefaultRecord' => false,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'positionProvider' => static::FIELD_SORT_ORDER_NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        static::FIELD_NAME_TEXT => $this->getTextFiled(1),
                        static::FIELD_IS_DELETE => $this->getIsDeleteFieldConfig(3)
                        //Add as many fields as you want
                    ]
                ]
            ]
        ];
    }
    protected function getTextFiled($sortOrder)
    {
        return [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label'         => __('Text Field'),
                            'componentType' => Field::NAME,
                            'formElement'   => Input::NAME,
                            'dataScope'     => 'textField',
                            'dataType'      => Text::NAME,
                            'sortOrder'     => $sortOrder
                        ],
                    ],
                ],
        ];
    }

    protected function getIsDeleteFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => ActionDelete::NAME,
                        'fit' => true,
                        'sortOrder' => $sortOrder
                    ],
                ],
            ],
        ];
    }
}
