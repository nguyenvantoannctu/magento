<?php

namespace Magenest\Movie\Ui\DataProvider\Product\Form\Modifier;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;

/**
 * Data provider for "Custom Attribute" field of product page
 */
class ModifierGroup extends AbstractModifier
{
    protected $locator;

    /**
     * @param ArrayManager                $arrayManager
     */
    public function __construct(
        ArrayManager $arrayManager,
        LocatorInterface $locator,
        UrlInterface $urlBuilder

    ) {
        $this->arrayManager = $arrayManager;
        $this->locator = $locator;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {

        if(!($this->locator->getProduct()->getTypeId() == 'virtual')){

            $meta = array_merge_recursive(
            $meta,
            ['fieldset_group_dynamics' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'visible' => false,
                            'disabled'=> true,
                        ]
                    ]
                ]
            ],

            ]);
        }
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * Customise Custom Attribute field
     *
     * @param array $meta
     *
     * @return array
     */

    protected function disabled(array $meta)
    {
        $fieldCode = 'fieldset_group_dynamics';

        $elementPath = $this->arrayManager->findPath($fieldCode, $meta, null, 'children');

        $containerPath = $this->arrayManager->findPath(static::CONTAINER_PREFIX . $fieldCode, $meta, null, 'children');

        if (!$elementPath) {
            return $meta;
        }
        $meta = $this->arrayManager->merge(
            $containerPath,
            $meta,
            [
                $fieldCode => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'visible' => false,
                                    'disabled'=> true,
                                ],
                            ],
                        ],
                    ]
            ]
        );


        return $meta;
    }
}
