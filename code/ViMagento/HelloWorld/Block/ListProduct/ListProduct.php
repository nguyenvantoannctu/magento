<?php

namespace ViMagento\HelloWorld\Block\ListProduct;
use Magento\Framework\View\Element\Template;


class ListProduct extends \Magento\Framework\View\Element\Template
{
    protected $postFactoryCollection;

    public function __construct(Template\Context $context,
                                     \ViMagento\HelloWorld\Model\PostFactory $postFactory,
                                array $data = []
    )
    {
        $this->postFactoryCollection = $postFactory;

        parent::__construct($context, $data);

    }

    public function getAll()
    {
        $collection  = $this->postFactoryCollection->create()->getCollection();
        return $collection;
    }
}
?>
